package com.example.duytu.danhba.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.activity.MainActivity;
import com.example.duytu.danhba.asynctask.UpdateAsyncTask;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.storeData.ContactManager;
import com.example.duytu.danhba.storeData.InsertData;

/*
 * Created by duytu on 6/20/2018.
 */

public class FragmentSuaThongTin extends Fragment implements Button.OnClickListener {

    private Button mBtnAddPhone;
    private Button mBtnAddEmail;
    private Button mBtnAddAddress;
    private Button mBtnAddOrganization;
    private Button mBtnAddBirth;
    private Button mBtnAddGroup;
    private Button mBtnBack;
    private Button mBtnOk;
    private EditText mEdtName;
    private ViewGroup mVgPhone;
    private ViewGroup mVgEmail;
    private ViewGroup mVgAddress;
    private ViewGroup mVgOrganization;
    private ViewGroup mVgGroup;
    private ViewGroup mVgBirthday;
    private LayoutInflater mLayoutInflate;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.update_contact, container, false);

        mBtnAddPhone = (Button) view.findViewById(R.id.btn_add_phone);
        mBtnAddEmail = (Button) view.findViewById(R.id.btn_add_email);
        mBtnAddAddress = (Button) view.findViewById(R.id.btn_add_address);
        mBtnAddOrganization = (Button) view.findViewById(R.id.btn_add_organization);
        mBtnAddBirth = (Button) view.findViewById(R.id.btn_add_birthday);
        mBtnAddGroup = (Button) view.findViewById(R.id.btn_add_group);
        mBtnBack = (Button) view.findViewById(R.id.btn_back);
        mBtnOk = (Button) view.findViewById(R.id.btn_ok);
        mEdtName = (EditText) view.findViewById(R.id.edt_name);

        Bundle bundle = getArguments();
        final Nguoi nguoi = (Nguoi) bundle.getSerializable("sua");
        final int pos = bundle.getInt("position");

        mVgPhone = (ViewGroup) view.findViewById(R.id.ll_add_phone);
        mVgEmail = (ViewGroup) view.findViewById(R.id.ll_add_email);
        mVgAddress = (ViewGroup) view.findViewById(R.id.ll_add_address);
        mVgOrganization = (ViewGroup) view.findViewById(R.id.ll_organization);
        mVgBirthday = (ViewGroup) view.findViewById(R.id.ll_birthday);
        mVgGroup = (ViewGroup) view.findViewById(R.id.ll_group);
        mLayoutInflate = (LayoutInflater) getActivity().getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mEdtName.setText(nguoi.getName());

        for (int i = 0; i < nguoi.getPhoneList().size(); i++) {
            final View view1 = mLayoutInflate.inflate(R.layout.add_phone, null);
            EditText data = (EditText) view1.findViewById(R.id.edt_data);
            Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
            ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

            data.setText(nguoi.getPhoneList().get(i).getNumber());

            String typePhone = nguoi.getPhoneList().get(i).getTypePhone();
            if (typePhone.toLowerCase().trim().equals("mobile")) {
                type.setSelection(0);
            } else if (typePhone.toLowerCase().trim().equals("home")) {
                type.setSelection(1);
            } else if (typePhone.toLowerCase().trim().equals("work")) {
                type.setSelection(2);
            } else type.setSelection(3);

            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewGroup) view1.getParent()).removeView(view1);
                }
            });
            mVgPhone.addView(view1, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.FILL_PARENT));
        }

        if (nguoi.getEmailList() != null && nguoi.getEmailList().size() > 0)
            for (int i = 0; i < nguoi.getEmailList().size(); i++) {
                final View view1 = mLayoutInflate.inflate(R.layout.add_data, null);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

                data.setText(nguoi.getEmailList().get(i).getAddress());
                String typePhone = nguoi.getEmailList().get(i).getType();
                if (typePhone.toLowerCase().trim().equals("home")) {
                    type.setSelection(0);
                } else if (typePhone.toLowerCase().trim().equals("work")) {
                    type.setSelection(1);
                } else type.setSelection(2);

                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ViewGroup) view1.getParent()).removeView(view1);
                    }
                });

                mVgEmail.addView(view1, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.FILL_PARENT));
            }
        if (nguoi.getAddressList() != null && nguoi.getAddressList().size() > 0) {
            for (int i = 0; i < nguoi.getAddressList().size(); i++) {
                final View view1 = mLayoutInflate.inflate(R.layout.add_data, null);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

                data.setText(nguoi.getAddressList().get(i).getAddress());
                String typeAddress = nguoi.getAddressList().get(i).getTypeAddress();
                if (typeAddress.toLowerCase().trim().equals("home")) {
                    type.setSelection(0);
                } else if (typeAddress.toLowerCase().trim().equals("work")) {
                    type.setSelection(1);
                } else type.setSelection(2);


                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ViewGroup) view1.getParent()).removeView(view1);
                    }
                });

                mVgAddress.addView(view1, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.FILL_PARENT));
            }
        }

        if (nguoi.getOrganization() != null) {
            final View view1 = mLayoutInflate.inflate(R.layout.add_line, null);
            EditText data = (EditText) view1.findViewById(R.id.edt_data);
            ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);
            data.setText(nguoi.getOrganization());
            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewGroup) view1.getParent()).removeView(view1);
                }
            });

            mVgOrganization.addView(view1, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.FILL_PARENT
            ));

        }

        if (nguoi.getBirthDay() != null) {
            final View view1 = mLayoutInflate.inflate(R.layout.add_line, null);
            EditText data = (EditText) view1.findViewById(R.id.edt_data);
            ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);
            data.setText(nguoi.getBirthDay());
            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewGroup) view1.getParent()).removeView(view1);
                }
            });

            mVgBirthday.addView(view1, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.FILL_PARENT
            ));

        }

        // Binding mVgGroup
        for (int i = 0; i < MainActivity.mExistGroup.size(); i++) {
            View view1 = mLayoutInflate.inflate(R.layout.item_group, null);
            CheckBox groupData = (CheckBox) view1.findViewById(R.id.cb_group);
            for (int j = 0; j < nguoi.getGroupList().size(); j++) {
                if (MainActivity.mExistGroup.get(i)
                        .getGroup_row_id().equals(nguoi.getGroupList().get(j)
                                .getGroup_row_id())) {
                    groupData.setChecked(true);
                }

            }
            groupData.setText(MainActivity.mExistGroup.get(i).getTitle());
            mVgGroup.addView(view1, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.FILL_PARENT
            ));
        }

        /*

         Chức năng của các button addNew

          */

        mBtnAddPhone.setOnClickListener(this);
        mBtnAddEmail.setOnClickListener(this);
        mBtnAddAddress.setOnClickListener(this);
        mBtnAddOrganization.setOnClickListener(this);
        mBtnAddBirth.setOnClickListener(this);
        mBtnAddGroup.setOnClickListener(this);

        /*
        Chức năng nút Back
         */

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Kiểm tra người dùng có muốn lưu thay đổi hay không

                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setTitle("Thông báo");
                dialog.setMessage("Bạn có muốn lưu các thay đổi ?");
                dialog.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        (new UpdateAsyncTask(getActivity(),getView(),getActivity().getSupportFragmentManager(),pos)).execute(nguoi);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.popBackStack();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (new UpdateAsyncTask(getActivity(),getView(),getActivity().getSupportFragmentManager(),pos)).execute(nguoi);
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_add_phone: {
                final View view1 = mLayoutInflate.inflate(R.layout.add_phone, null);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ViewGroup) view1.getParent()).removeView(view1);
                    }
                });

                mVgPhone.addView(view1, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.FILL_PARENT
                ));
            }
            break;
            case R.id.btn_add_email: {
                final View view1 = mLayoutInflate.inflate(R.layout.add_data, null);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ViewGroup) view1.getParent()).removeView(view1);
                    }
                });

                mVgEmail.addView(view1, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.FILL_PARENT
                ));
            }
            break;
            case R.id.btn_add_address: {
                final View view1 = mLayoutInflate.inflate(R.layout.add_data, null);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ViewGroup) view1.getParent()).removeView(view1);
                    }
                });

                mVgAddress.addView(view1, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.FILL_PARENT
                ));
            }
            break;
            case R.id.btn_add_organization: {
                if (mVgOrganization.getChildCount() == 0) {
                    final View view1 = mLayoutInflate.inflate(R.layout.add_line, null);
                    EditText data = (EditText) view1.findViewById(R.id.edt_data);
                    ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

                    remove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((ViewGroup) view1.getParent()).removeView(view1);
                        }
                    });

                    mVgOrganization.addView(view1, new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.FILL_PARENT
                    ));
                }
            }
            break;
            case R.id.btn_add_birthday: {
                if (mVgBirthday.getChildCount() == 0) {
                    final View view1 = mLayoutInflate.inflate(R.layout.add_line, null);
                    EditText data = (EditText) view1.findViewById(R.id.edt_data);
                    ImageView remove = (ImageView) view1.findViewById(R.id.iv_remove);

                    remove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((ViewGroup) view1.getParent()).removeView(view1);
                        }
                    });

                    mVgBirthday.addView(view1, new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.FILL_PARENT
                    ));
                }
            }
            break;
            case R.id.btn_add_group: {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.dialog_add_group);
                final EditText nameGroup = dialog.findViewById(R.id.edt_groups_name);
                Button quit = dialog.findViewById(R.id.btn_quit);
                Button ok = dialog.findViewById(R.id.btn_ok);

                quit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (nameGroup.getText().toString().isEmpty()) {
                            AlertDialog.Builder message = new AlertDialog.Builder(getContext());
                            message.setTitle("Thông báo");
                            message.setMessage("Bạn chưa điền tên nhóm");
                            message.setPositiveButton("Quay lại",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            message.show();
                        } else {
                            InsertData insertData = new InsertData(getContext());
                            insertData.addNewGroup(nameGroup.getText().toString());
                            ContactManager contactManager = new ContactManager(getContext());
                            MainActivity.mExistGroup = contactManager.getGroup();
                            /*
                            Reload mVgGroup view
                             */

                            dialog.dismiss();
                        }

                    }
                });

                dialog.show();
            }
            break;

        }
    }
}

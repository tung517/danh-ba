package com.example.duytu.danhba.storeData;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.widget.Toast;

import com.example.duytu.danhba.model.Address;
import com.example.duytu.danhba.model.Email;
import com.example.duytu.danhba.model.Group;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.model.Phone;

import java.util.ArrayList;
import java.util.List;

import static android.provider.ContactsContract.Contacts.Entity.RAW_CONTACT_ID;
import static com.example.duytu.danhba.R.drawable.custom_button;
import static com.example.duytu.danhba.R.drawable.nguoi;


/**
 * Created by duytu on 6/18/2018.
 */

public class InsertData {

    Context mContext;

    public InsertData(Context mContext) {
        this.mContext = mContext;
    }

    public Nguoi insertNewContact(Nguoi nguoi) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();

        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        // Thêm tên liên hệ
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                        nguoi.getName())
                .build());

        // Thêm số điện thoại
        for (int i = 0; i < nguoi.getPhoneList().size(); i++) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
                            nguoi.getPhoneList().get(i).getNumber())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            nguoi.getPhoneList().get(i).changeType())
                    .build());
        }

        // Thêm địa chỉ email
        if (nguoi.getEmailList() != null && nguoi.getEmailList().size() > 0)
            for (int i = 0; i < nguoi.getEmailList().size(); i++) {
                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS,
                                nguoi.getEmailList().get(i).getAddress())
                        .withValue(ContactsContract.CommonDataKinds.Email.TYPE,
                                nguoi.getEmailList().get(i).changeType())
                        .build());
            }

        // Thêm địa chỉ
        if (nguoi.getAddressList() != null && nguoi.getAddressList().size() > 0)
            for (int i = 0; i < nguoi.getAddressList().size(); i++) {
                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds
                                        .StructuredPostal.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.StructuredPostal.STREET,
                                nguoi.getAddressList().get(i).getAddress())
                        .withValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE,
                                nguoi.getAddressList().get(i).changeType())
                        .build());
            }

        // Thêm công ty
        if (nguoi.getOrganization() != null)
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY,
                            nguoi.getOrganization())
                    .build());
        //Thêm nhóm
        if (nguoi.getGroupList() != null && nguoi.getGroupList().size() > 0)
            for (int i = 0; i < nguoi.getGroupList().size(); i++) {
                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds
                                        .GroupMembership.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds
                                        .GroupMembership.GROUP_ROW_ID,
                                nguoi.getGroupList().get(i).getGroup_row_id())
                        .build());
            }


        // Thêm ngày sinh
        if (nguoi.getBirthDay() != null)
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Event.START_DATE,
                            nguoi.getBirthDay())
                    .build());
        try {
            mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }

//         Lấy raw id

        String where = ContactsContract.Data.DISPLAY_NAME + "= '"
                + nguoi.getName().trim() + "'";

        Cursor cursor = mContext.getContentResolver().query(
                ContactsContract.Data.CONTENT_URI,
                new String[]{ContactsContract.Data.RAW_CONTACT_ID},
                where, null, null);
        String RAW_CONTACT_ID = null;
        while (cursor.moveToNext()) {
            RAW_CONTACT_ID = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.Data.RAW_CONTACT_ID));
        }
        nguoi.setRawId(RAW_CONTACT_ID);
        return nguoi;
    }


    public ArrayList<ContentProviderOperation> insertPhone(
            Nguoi nguoi, Phone phone, ArrayList<ContentProviderOperation> ops) {

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID,
                        Integer.parseInt(nguoi.getRawId()))
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
                        phone.getNumber())
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                        phone.changeType())
                .build());


        return ops;
    }

    public ArrayList<ContentProviderOperation> insertEmail(
            Nguoi nguoi, Email email, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID,
                        Integer.parseInt(nguoi.getRawId()))
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS,
                        email.getAddress())
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE,
                        email.changeType())
                .build());
        return ops;
    }


    public ArrayList<ContentProviderOperation> insertAddress(
            Nguoi nguoi, Address address, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID,
                        Integer.parseInt(nguoi.getRawId()))
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.STREET,
                        address.getAddress())
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE,
                        address.changeType())
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> insertOrganization(
            Nguoi nguoi, String organization, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID,
                        Integer.parseInt(nguoi.getRawId()))
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY,
                        organization)
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> insertBirthDay(
            Nguoi nguoi, String birthday, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID,
                        Integer.parseInt(nguoi.getRawId()))
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Event.START_DATE, birthday)
                .build());

        return ops;
    }


    public ArrayList<ContentProviderOperation> insertGroup(
            Nguoi nguoi, Group group, ArrayList<ContentProviderOperation> ops) {


        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID,
                        Integer.parseInt(nguoi.getRawId()))
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds
                                .GroupMembership.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID,
                        group.getGroup_row_id())
                .build());
        return ops;

    }


    public void addNewGroup(String group) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Groups.CONTENT_URI)
                .withValue(ContactsContract.Groups.TITLE, group)
                .build());
        try {
            mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }


}

package com.example.duytu.danhba.storeData;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.widget.Toast;

import com.example.duytu.danhba.model.Address;
import com.example.duytu.danhba.model.Email;
import com.example.duytu.danhba.model.Group;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.model.Phone;

import java.util.ArrayList;

import static android.R.attr.type;
import static com.example.duytu.danhba.R.layout.group;

/**
 * Created by duytu on 6/19/2018.
 */

public class DeleteData {

    public ArrayList<ContentProviderOperation> deletePhone
            (Nguoi nguoi, int index, ArrayList<ContentProviderOperation> ops) {

        ops.add(ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Phone.NUMBER + "= ?",
                        new String[]{nguoi.getPhoneList().get(index).getNumber()})
                .build());
        return ops;

    }

    public ArrayList<ContentProviderOperation> deleteEmail(
            Nguoi nguoi, int index, ArrayList<ContentProviderOperation> ops) {

        ops.add(ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Email.ADDRESS + "= ?",
                        new String[]{nguoi.getEmailList().get(index).getAddress()})
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> deleteAddress(
            Nguoi nguoi, int index, ArrayList<ContentProviderOperation> ops) {

        ops.add(ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds
                                .StructuredPostal.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds
                                .StructuredPostal.STREET + "= ?",
                        new String[]{nguoi.getAddressList().get(index).getAddress()})
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> deleteOrganization(
            Nguoi nguoi, ArrayList<ContentProviderOperation> ops) {

        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds
                                .Organization.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Organization.COMPANY + "= ?",
                        new String[]{nguoi.getOrganization()})
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> deleteBirthday(
            Nguoi nguoi, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Event.START_DATE + "= ?",
                        new String[]{nguoi.getBirthDay()})
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> deleteGroup(
            Nguoi nguoi, String group_row_id, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?", new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds
                                .GroupMembership.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds
                                .GroupMembership.GROUP_ROW_ID + "= ?",
                        new String[]{group_row_id})
                .build());
        return ops;
    }

}

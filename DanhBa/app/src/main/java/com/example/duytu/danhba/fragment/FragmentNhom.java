package com.example.duytu.danhba.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.activity.MainActivity;
import com.example.duytu.danhba.adapter.RecyclerMultiViewAdapter;
import com.example.duytu.danhba.interfaces.OnFavouriteClickListener;
import com.example.duytu.danhba.model.Model;

import java.util.ArrayList;
import java.util.List;

import static com.example.duytu.danhba.activity.MainActivity.mNguoiList;

/**
 * Created by duytu on 5/8/2018.
 */

public class FragmentNhom extends Fragment {

    List<Model> modelList = new ArrayList<>();
    RecyclerView rvList;
    RecyclerMultiViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group, container, false);

        if (modelList.size() > 0){
            modelList = new ArrayList<>();
        }

        for (int i = 0; i < MainActivity.mExistGroup.size(); i++){
            modelList = Model.groupMember(
                    MainActivity.mExistGroup.get(i).getTitle(),modelList, mNguoiList);
        }

        rvList = (RecyclerView) view.findViewById(R.id.recyclerView);
        adapter = new RecyclerMultiViewAdapter(getContext(),modelList);

        adapter.setOnFavouriteClickListener(new OnFavouriteClickListener() {
            @Override
            public void onClick(View view, int position) {
                FragmentChiTiet chiTietFragment = new FragmentChiTiet();
                Bundle bundle = new Bundle();
                bundle.putSerializable("chitiet", modelList.get(position).getNguoi());
                chiTietFragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, chiTietFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        rvList.setAdapter(adapter);
        rvList.setLayoutManager(new LinearLayoutManager
                (getContext(),LinearLayoutManager.VERTICAL,false));
        rvList.setItemAnimator(new DefaultItemAnimator());

        return view;
    }
}

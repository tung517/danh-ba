package com.example.duytu.danhba.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.activity.MainActivity;
import com.example.duytu.danhba.model.Nguoi;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.duytu.danhba.activity.MainActivity.mNguoiList;
import static com.example.duytu.danhba.activity.MainActivity.mViewPager;


/**
 * Created by duytu on 6/4/2018.
 */

public class FragmentChiTiet extends Fragment {



    private CircleImageView mTvImage;
    private ImageView mTvBack;
    private ImageView mTvFavourite;
    private ImageView mTvOption;
    private TextView mTvName;
    private Toolbar mToolbar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail, container, false);

        mTvImage = (CircleImageView) view.findViewById(R.id.iv_image);
        mTvFavourite = (ImageView) view.findViewById(R.id.iv_favourite);
        mTvOption = (ImageView) view.findViewById(R.id.iv_option);
        mTvBack = (ImageView) view.findViewById(R.id.iv_back);
        mTvName = (TextView) view.findViewById(R.id.tv_name);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        mTvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                ((MainActivity) getActivity()).setAdapter(mViewPager);
                fragmentManager.popBackStack();
            }
        });

        Bundle bundle = getArguments();
        final Nguoi nguoi = (Nguoi) bundle.getSerializable("chitiet");

        if (nguoi.getPhoto() == null) {
            Picasso.with(getContext()).load(R.drawable.user_2).into(mTvImage);
        } else {
            Picasso.with(getContext()).load(nguoi.getPhoto()).into(mTvImage);
        }

        if (!nguoi.getFavourite()) {
            mTvFavourite.setImageResource(R.drawable.star_non_favourite);
        } else {
            mTvFavourite.setImageResource(R.drawable.star_favourite);
        }

        mTvFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nguoi.getFavourite()) {
                    for (Nguoi ng : mNguoiList) {
                        if (ng.equals(nguoi)) {
                            ng.setFavourite(false);
//                            UpdateData updateData = new UpdateData();
//                            updateData.updateFavourite(ng);
                        }
                    }
                    mTvFavourite.setImageResource(R.drawable.non_favourite);
                } else {
                    for (Nguoi ng : mNguoiList) {
                        if (ng.equals(nguoi)) {
                            ng.setFavourite(true);
                        }
                    }
                    mTvFavourite.setImageResource(R.drawable.star_favourite);
                }

            }
        });

        mTvName.setText(nguoi.getName());

        LayoutInflater vi = (LayoutInflater) getActivity().getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < nguoi.getPhoneList().size(); i++) {
            View v = vi.inflate(R.layout.item_phone, null);
            TextView tvData = (TextView) v.findViewById(R.id.tv_data);
            TextView tvType = (TextView) v.findViewById(R.id.tv_type);
            tvData.setText(nguoi.getPhoneList().get(i).getNumber());
            tvType.setText(nguoi.getPhoneList().get(i).getTypePhone());
            ViewGroup insertPoint = (ViewGroup) view.findViewById(R.id.ll_phone_list);
            insertPoint.addView(v, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.FILL_PARENT));
        }

        if (nguoi.getAddressList() != null) {
            if (nguoi.getAddressList().size() > 0) {
                for (int i = 0; i < nguoi.getAddressList().size(); i++) {
                    View v = vi.inflate(R.layout.item_data, null);
                    TextView tvData = (TextView) v.findViewById(R.id.tv_data);
                    TextView tvType = (TextView) v.findViewById(R.id.tv_type);
                    tvData.setText(nguoi.getAddressList().get(i).getAddress());
                    tvType.setText(nguoi.getAddressList().get(i).getTypeAddress());
                    ViewGroup insertPoint = (ViewGroup) view.findViewById(R.id.ll_adrress_list);
                    insertPoint.addView(v, new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.FILL_PARENT));
                }
            }
        }

        if (nguoi.getEmailList() != null)
            if (nguoi.getEmailList().size() > 0) {
                for (int i = 0; i < nguoi.getEmailList().size(); i++) {
                    View v = vi.inflate(R.layout.item_data, null);
                    TextView tvData = (TextView) v.findViewById(R.id.tv_data);
                    TextView tvType = (TextView) v.findViewById(R.id.tv_type);
                    tvData.setText(nguoi.getEmailList().get(i).getAddress());
                    tvType.setText(nguoi.getEmailList().get(i).getType());
                    ViewGroup insertPoint = (ViewGroup) view.findViewById(R.id.ll_email_list);
                    insertPoint.addView(v, new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.FILL_PARENT));
                }
            }

        if (nguoi.getOrganization() != null) {
            View v = vi.inflate(R.layout.item_other, null);
            TextView tvData = (TextView) v.findViewById(R.id.tv_other);
            tvData.setText(nguoi.getOrganization());
            ViewGroup insertPoint = (ViewGroup) view.findViewById(R.id.ll_organization);
            insertPoint.addView(v, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        }
        if (nguoi.getBirthDay() != null) {
            View v = vi.inflate(R.layout.item_other, null);
            TextView tvData = (TextView) v.findViewById(R.id.tv_other);
            tvData.setText(nguoi.getBirthDay());
            ViewGroup insertPoint = (ViewGroup) view.findViewById(R.id.ll_birthday);
            insertPoint.addView(v, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        }

        if (nguoi.getGroupList() != null)
            if (nguoi.getGroupList().size() > 0) {
                StringBuffer buffer = new StringBuffer();
                for (int i = 0; i < nguoi.getGroupList().size(); i++) {
                    if (i != nguoi.getGroupList().size() - 1)
                        buffer.append(nguoi.getGroupList().get(i).getTitle() + "\n");
                    else {
                        buffer.append(nguoi.getGroupList().get(i).getTitle());
                    }
                }
                View v = vi.inflate(R.layout.item_other, null);
                TextView tvData = (TextView) v.findViewById(R.id.tv_other);
                tvData.setText(buffer.toString());
                ViewGroup insertPoint = (ViewGroup) view.findViewById(R.id.ll_group);
                insertPoint.addView(v, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.FILL_PARENT));
            }

        return view;
    }

}

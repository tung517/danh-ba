package com.example.duytu.danhba.model;

import android.provider.ContactsContract;

/**
 * Created by duytu on 6/10/2018.
 */

public class Phone {
    private String number;
    private String typePhone;
    private boolean delete;

    public Phone() {
    }

    public Phone(String number, String typePhone) {
        this.number = number;
        this.typePhone = typePhone;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTypePhone() {
        return typePhone;
    }

    public void setTypePhone(String typePhone) {
        this.typePhone = typePhone;
    }

    public int changeType() {
        if (this.typePhone.trim().toLowerCase().equals("home".trim())) {
            return ContactsContract.CommonDataKinds.Phone.TYPE_HOME;
        } else if (this.typePhone.trim().toLowerCase().equals("work".trim())) {
            return ContactsContract.CommonDataKinds.Phone.TYPE_WORK;
        } else if (this.typePhone.trim().toLowerCase().equals("mobile".trim())) {
            return ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;
        } else {
            return ContactsContract.CommonDataKinds.Phone.TYPE_OTHER;
        }
    }
}

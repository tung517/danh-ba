package com.example.duytu.danhba.model;

/**
 * Created by duytu on 6/17/2018.
 */

public class Group {
    private String dataId;
    private String title;
    private String group_row_id;


    public Group(String title, String group_row_id) {
        this.title = title;
        this.group_row_id = group_row_id;
    }

    public Group(String dataId, String title, String group_row_id) {
        this.dataId = dataId;
        this.title = title;
        this.group_row_id = group_row_id;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getGroup_row_id() {
        return group_row_id;
    }

    public void setGroup_row_id(String group_row_id) {
        this.group_row_id = group_row_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

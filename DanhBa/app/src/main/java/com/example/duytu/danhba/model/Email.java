package com.example.duytu.danhba.model;

import android.provider.ContactsContract;

/**
 * Created by duytu on 6/10/2018.
 */

public class Email {
    private String address;
    private String type;

    public Email() {
    }

    public Email(String id, String address, String type) {
        this.address = address;
        this.type = type;
    }

    public Email(String address, String type) {
        this.address = address;
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int changeType(){
        if (this.type.trim().toLowerCase().equals("Home".toLowerCase())){
            return ContactsContract.CommonDataKinds.Email.TYPE_HOME;
        }
        else if (this.type.trim().toLowerCase().equals("Work".toLowerCase())){
            return ContactsContract.CommonDataKinds.Email.TYPE_WORK;
        }
        else {
            return ContactsContract.CommonDataKinds.Email.TYPE_OTHER;
        }
    }
}

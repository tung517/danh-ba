package com.example.duytu.danhba.model;

import android.provider.ContactsContract;

/**
 * Created by duytu on 6/10/2018.
 */

public class Address {

    private String address;
    private String typeAddress;

    public Address() {
    }

    public Address(String id, String address, String typeAddress) {
        this.address = address;
        this.typeAddress = typeAddress;
    }

    public Address(String address, String typeAddress) {
        this.address = address;
        this.typeAddress = typeAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeAddress() {
        return typeAddress;
    }

    public void setTypeAddress(String typeAddress) {
        this.typeAddress = typeAddress;
    }

    public int changeType(){
        if (this.typeAddress.trim().toLowerCase().equals("Home".toLowerCase())){
            return ContactsContract.CommonDataKinds.Email.TYPE_HOME;
        }
        else if (this.typeAddress.trim().toLowerCase().equals("Work".toLowerCase())){
            return ContactsContract.CommonDataKinds.Email.TYPE_WORK;
        }
        else {
            return ContactsContract.CommonDataKinds.Email.TYPE_OTHER;
        }
    }
}

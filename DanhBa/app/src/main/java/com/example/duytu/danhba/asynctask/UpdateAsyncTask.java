package com.example.duytu.danhba.asynctask;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.activity.MainActivity;
import com.example.duytu.danhba.model.Address;
import com.example.duytu.danhba.model.Email;
import com.example.duytu.danhba.model.Group;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.model.Phone;
import com.example.duytu.danhba.storeData.DeleteData;
import com.example.duytu.danhba.storeData.InsertData;
import com.example.duytu.danhba.storeData.UpdateData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duytu on 6/25/2018.
 */

public class UpdateAsyncTask extends AsyncTask<Nguoi, Void, Nguoi> {

    Context mContext;
    View mView;
    FragmentManager fragmentManager;
    int position;

    public UpdateAsyncTask(Context mContext, View mView, FragmentManager fragmentManager, int position) {
        this.mContext = mContext;
        this.mView = mView;
        this.fragmentManager = fragmentManager;
        this.position = position;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Nguoi nguoi) {
        super.onPostExecute(nguoi);
        Toast.makeText(mContext, "Suscess", Toast.LENGTH_SHORT).show();
        MainActivity.mNguoiList.set(position, nguoi);
        fragmentManager.popBackStack();
        ((MainActivity) mContext).setAdapter(MainActivity.mViewPager);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Nguoi doInBackground(Nguoi... params) {

        Nguoi nguoi = params[0];

        UpdateData updateData = new UpdateData();
        InsertData insertData = new InsertData(mContext);
        DeleteData deleteData = new DeleteData();

        ArrayList<ContentProviderOperation> ops =
                new ArrayList<ContentProviderOperation>();

        ViewGroup mVgPhone = (ViewGroup) mView.findViewById(R.id.ll_add_phone);
        ViewGroup mVgEmail = (ViewGroup) mView.findViewById(R.id.ll_add_email);
        ViewGroup mVgAddress = (ViewGroup) mView.findViewById(R.id.ll_add_address);
        ViewGroup mVgOrganization = (ViewGroup) mView.findViewById(R.id.ll_organization);
        ViewGroup mVgBirthday = (ViewGroup) mView.findViewById(R.id.ll_birthday);
        ViewGroup mVgGroup = (ViewGroup) mView.findViewById(R.id.ll_group);
        LayoutInflater mLayoutInflate = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Cập nhật tên

        EditText mEdtName = (EditText) mView.findViewById(R.id.edt_name);
        if (!nguoi.getName().equals(mEdtName.getText().toString())) {
            ops = updateData.updateName(nguoi,
                    mEdtName.getText().toString(), ops);
            nguoi.setName(mEdtName.getText().toString());
        }


        // Cập nhật số điện thoại
        // Trường hợp thêm số điện thoại vượt qua số lượng số điện thoại trước đó
        if (mVgPhone.getChildCount() > nguoi.getPhoneList().size()) {
            List<Phone> phoneList = new ArrayList<Phone>();
            for (int i = 0; i < mVgPhone.getChildCount(); i++) {
                View view1 = mVgPhone.getChildAt(i);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                Phone phone1 = new Phone(
                        data.getText().toString(),
                        type.getSelectedItem().toString());
                if (i >= nguoi.getPhoneList().size()) {
                    ops = insertData.insertPhone(nguoi, phone1, ops);
                } else {
                    ops = updateData.updatePhone(nguoi, i, phone1, ops);
                }
                phoneList.add(phone1);
            }
            nguoi.setPhoneList(phoneList);

        }
        // Trường hợp số lượng số điện thoại ít hơn ban đầu
        else {
            List<Phone> phoneList = new ArrayList<Phone>();

            int newNumber = mVgPhone.getChildCount();
            int oldNumber = nguoi.getPhoneList().size();

            if (oldNumber > newNumber) {
                for (int i = newNumber; i < oldNumber; i++) {
                    ops = deleteData.deletePhone(nguoi, i, ops);
                }
            }

            for (int i = 0; i < newNumber; i++) {
                View view1 = mVgPhone.getChildAt(i);
                EditText data = (EditText) view1.findViewById(
                        R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                Phone phone1 = new Phone(
                        data.getText().toString(),
                        type.getSelectedItem().toString());
                ops = updateData.updatePhone(nguoi, i, phone1, ops);
                phoneList.add(phone1);
            }
            nguoi.setPhoneList(phoneList);
        }

        // Cập nhật mVgEmail
        // Trường hợp thêm mVgEmail vượt qua số lượng mVgEmail trước đó
        if (mVgEmail.getChildCount() > nguoi.getEmailList().size()) {
            List<Email> emailList = new ArrayList<Email>();
            for (int i = 0; i < mVgEmail.getChildCount(); i++) {
                View view1 = mVgEmail.getChildAt(i);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                Email email1 = new Email(
                        data.getText().toString(),
                        type.getSelectedItem().toString());

                if (i >= nguoi.getEmailList().size()) {
                    ops = insertData.insertEmail(nguoi, email1, ops);
                } else {
                    ops = updateData.updateEmail(nguoi, i, email1, ops);
                }

                emailList.add(email1);
            }
            nguoi.setEmailList(emailList);
        }
        // Trường hợp số lượng mVgEmail ít hơn ban đầu
        else {
            List<Email> emailList = new ArrayList<Email>();

            int newNumber = mVgEmail.getChildCount();
            int oldNumber = nguoi.getEmailList().size();

            if (oldNumber > newNumber) {
                for (int i = newNumber; i < oldNumber; i++) {
                    ops = deleteData.deleteEmail(nguoi, i, ops);
                }
            }

            for (int i = 0; i < newNumber; i++) {
                View view1 = mVgEmail.getChildAt(i);
                EditText data = (EditText) view1.findViewById(
                        R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                Email email1 = new Email(
                        data.getText().toString(),
                        type.getSelectedItem().toString());
                ops = updateData.updateEmail(nguoi, i, email1, ops);
                emailList.add(email1);
            }
            nguoi.setEmailList(emailList);
        }

        // Cập nhật địa chỉ
        // Trường hợp thêm địa chỉ vượt qua số lượng địa chỉ trước đó
        if (mVgAddress.getChildCount() > nguoi.getAddressList().size()) {
            List<Address> addressList = new ArrayList<Address>();
            for (int i = 0; i < mVgAddress.getChildCount(); i++) {
                View view1 = mVgAddress.getChildAt(i);
                EditText data = (EditText) view1.findViewById(R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);
                Address address1 = new Address(
                        data.getText().toString(),
                        type.getSelectedItem().toString());
                if (i >= nguoi.getAddressList().size()) {
                    ops = insertData.insertAddress(nguoi, address1, ops);
                } else {
                    ops = updateData.updateAddress
                            (nguoi, i, address1, ops);
                }

                addressList.add(address1);
            }
            nguoi.setAddressList(addressList);

        }
        // Trường hợp số lượng mVgAddress ít hơn ban đầu
        else {
            List<Address> addressList = new ArrayList<Address>();
            int newNumber = mVgAddress.getChildCount();
            int oldNumber = nguoi.getAddressList().size();

            if (oldNumber > newNumber) {
                for (int i = newNumber; i < oldNumber; i++) {
                    ops = deleteData.deleteAddress(nguoi, i, ops);
                }
            }

            for (int i = 0; i < newNumber; i++) {
                View view1 = mVgAddress.getChildAt(i);
                EditText data = (EditText) view1.findViewById(
                        R.id.edt_data);
                Spinner type = (Spinner) view1.findViewById(R.id.spn_type);

                Address address1 = new Address(
                        data.getText().toString(),
                        type.getSelectedItem().toString());
                ops = updateData.updateAddress(nguoi, i, address1, ops);
                addressList.add(address1);
            }
            nguoi.setAddressList(addressList);
        }

        // Cập nhật công ty
        // Trường hợp thêm mới
        if (mVgOrganization.getChildCount() > 0
                && nguoi.getOrganization() == null) {
            View view1 = mVgOrganization.getChildAt(0);
            EditText data = view1.findViewById(R.id.edt_data);
            ops = insertData.insertOrganization(nguoi,
                    data.getText().toString(), ops);
            nguoi.setOrganization(data.getText().toString().trim());
        }
        // Trường hợp sửa
        else if (mVgOrganization.getChildCount() > 0
                && nguoi.getOrganization() != null) {
            View view1 = mVgOrganization.getChildAt(0);
            EditText data = (EditText) view1.findViewById(R.id.edt_data);
            if (!data.getText().toString().trim()
                    .equals(nguoi.getOrganization().trim())) {
                ops = updateData.updateOrganization(nguoi,
                        data.getText().toString(), ops);
                nguoi.setOrganization(data.getText().toString().trim());
            }
        } else if (mVgOrganization.getChildCount() == 0
                && nguoi.getOrganization() != null) {
            ops = deleteData.deleteOrganization(nguoi, ops);
            nguoi.setOrganization(null);
        }

        // Cập nhật ngày sinh
        // Trường hợp thêm mới
        if (mVgBirthday.getChildCount() > 0
                && nguoi.getBirthDay() == null) {
            View view1 = mVgBirthday.getChildAt(0);
            EditText data = (EditText) view1.findViewById(R.id.edt_data);
            ops = insertData.insertBirthDay(nguoi, data.getText().toString(), ops);
            nguoi.setBirthDay(data.getText().toString().trim());
        }
        // Trường hợp sửa
        else if (mVgBirthday.getChildCount() > 0
                && nguoi.getBirthDay() != null) {
            View view1 = mVgBirthday.getChildAt(0);
            EditText data = (EditText) view1.findViewById(R.id.edt_data);
            if (!data.getText().toString().trim()
                    .equals(nguoi.getBirthDay().trim())) {
                ops = updateData.updateBirthday(nguoi,
                        data.getText().toString().trim(), ops);
                nguoi.setBirthDay(data.getText().toString().trim());
            }
        }
        // Trường hợp xóa
        else if (mVgBirthday.getChildCount() == 0
                && nguoi.getBirthDay() != null) {
            ops = deleteData.deleteBirthday(nguoi, ops);
            nguoi.setBirthDay(null);
        }

        // Cập nhật nhóm
        List<Group> groupList = new ArrayList<Group>();
        for (int i = 0; i < mVgGroup.getChildCount(); i++) {
            View view1 = mVgGroup.getChildAt(i);
            CheckBox data = (CheckBox) view1.findViewById(R.id.cb_group);
            if (data.isChecked()) {
                boolean flag = false;
                for (Group gr : nguoi.getGroupList()) {
                    if (data.getText().toString().trim()
                            .equals(gr.getTitle())) {
                        flag = true;
                        break;
                    }
                }
                // Trường hợp thêm mới
                if (flag == false) {
                    for (int j = 0; j < MainActivity.mExistGroup.size(); j++) {
                        if (MainActivity.mExistGroup.get(j).getTitle()
                                .equals(data.getText().toString().trim())) {
                            nguoi.getGroupList().add(MainActivity
                                    .mExistGroup.get(j));
                            ops = updateData.insertGroup(nguoi, MainActivity
                                    .mExistGroup.get(j).getGroup_row_id(), ops);
                            break;
                        }
                    }
                }
            } else {
                boolean flag = false;
                for (Group gr : nguoi.getGroupList()) {
                    if (data.getText().toString().trim()
                            .equals(gr.getTitle())) {
                        flag = true;
                        break;
                    }
                }
                // Trường hợp xóa nhóm
                if (flag == true) {
                    for (int j = 0; j < MainActivity.mExistGroup.size(); j++) {
                        if (MainActivity.mExistGroup.get(j).getTitle()
                                .equals(data.getText().toString().trim())) {
                            ops = deleteData.deleteGroup(nguoi, MainActivity
                                    .mExistGroup.get(j).getGroup_row_id(), ops);
                            for (int k = 0; k < nguoi.getGroupList().size(); k++) {
                                if (nguoi.getGroupList().get(k).getGroup_row_id()
                                        .equals(MainActivity.mExistGroup.get(j)
                                                .getGroup_row_id())) {
                                    nguoi.getGroupList().remove(k);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        try {
            mContext.getContentResolver()
                    .applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
        return nguoi;
    }
}

package com.example.duytu.danhba.storeData;

import android.provider.ContactsContract;

/**
 * Created by duytu on 6/29/2018.
 */

public class ConstantString {

    public static final String NAME_WHERE = ContactsContract.CommonDataKinds.StructuredName.MIMETYPE
            + "= '" + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
            + "' and " + ContactsContract.Data.CONTACT_ID
            + " = ";

    public static final String PHONE_WHERE = ContactsContract.CommonDataKinds.Phone.MIMETYPE
            + "= '" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
            + "' and " + ContactsContract.Data.CONTACT_ID
            + " = ";

    public static final String EMAIL_WHERE = ContactsContract.CommonDataKinds.Email.MIMETYPE
            + "= '" + ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE
            + "' and " + ContactsContract.Data.CONTACT_ID
            + " = ";

    public static final String ADDRESS_WHERE = ContactsContract.CommonDataKinds.StructuredPostal.MIMETYPE
            + "= '" + ContactsContract.CommonDataKinds
            .StructuredPostal.CONTENT_ITEM_TYPE
            + "' and " + ContactsContract.Data.CONTACT_ID
            + " = ";

    public static final String ORGANIZATION_WHERE = ContactsContract.CommonDataKinds.Organization.MIMETYPE
            + "= '" + ContactsContract.CommonDataKinds
            .Organization.CONTENT_ITEM_TYPE
            + "' and " + ContactsContract.Data.CONTACT_ID
            + " = ";

    public static final String BIRTHDAY_WHERE = ContactsContract.Data.MIMETYPE + "= '"
            + ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE + "' and "
            + ContactsContract.Data.CONTACT_ID + "=";

    public static final String FAVOURITE_WHERE = ContactsContract.Contacts.STARRED + "='1'" +
            " and " + ContactsContract.Contacts._ID + "=";

    public static final String GROUP_WHERE = ContactsContract.Data.MIMETYPE + "= '" +
            ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE +
            "' and " + ContactsContract.Data.CONTACT_ID + "=";

    public static final String[] NAME_COLUMN = {
            ContactsContract.Data._ID,
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.Data.RAW_CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME};

    public static final String[] PHONE_COLUMN = {
            ContactsContract.Data._ID,
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.Data.RAW_CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.TYPE,
            ContactsContract.CommonDataKinds.Phone.PHOTO_URI};

    public static final String[] EMAIL_COLUMN = {
            ContactsContract.Data._ID,
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.CommonDataKinds.Email.ADDRESS,
            ContactsContract.CommonDataKinds.Email.TYPE};

    public static final String[] ADDRESS_COLUMN = {
            ContactsContract.Data._ID,
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredPostal.STREET,
            ContactsContract.CommonDataKinds.StructuredPostal.TYPE};

    public static final String[] ORGANIZATION_COLUMN = {
            ContactsContract.Data._ID,
            ContactsContract.Data.CONTACT_ID,
            ContactsContract.CommonDataKinds.Organization.COMPANY,};

    public static final String[] BIRTHDAY_COLUMN = {
            ContactsContract.CommonDataKinds.Event.START_DATE,
            ContactsContract.Data._ID};
}

package com.example.duytu.danhba.model;

import com.example.duytu.danhba.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duytu on 6/26/2018.
 */

public class Model {

    public static final int TYPE_NGUOI = 0;
    public static final int TYPE_GROUP = 1;

    private Nguoi nguoi;
    private String title;
    private int type;

    public Model(int type, String title, Nguoi nguoi) {
        this.nguoi = nguoi;
        this.title = title;
        this.type = type;
    }

    public static List<Model> groupMember(
            String title, List<Model> modelList, List<Nguoi> nguoiList)
    {
        modelList.add(new Model(Model.TYPE_GROUP, title, null));
        for (Nguoi nguoi : nguoiList) {
            for (int i = 0; i < nguoi.getGroupList().size(); i++) {
                if (nguoi.getGroupList().get(i).getTitle().equals(title)) {
                    modelList.add(new Model(Model.TYPE_NGUOI, null, nguoi));
                }
            }
        }
        return modelList;
    }

    public void getModelList() {

    }

    public Nguoi getNguoi() {
        return nguoi;
    }

    public void setNguoi(Nguoi nguoi) {
        this.nguoi = nguoi;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

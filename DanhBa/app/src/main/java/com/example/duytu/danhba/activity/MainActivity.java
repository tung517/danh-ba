package com.example.duytu.danhba.activity;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.duytu.danhba.fragment.FragmentDanhBa;
import com.example.duytu.danhba.fragment.FragmentNhom;
import com.example.duytu.danhba.R;
import com.example.duytu.danhba.adapter.ViewPagerAdapter;
import com.example.duytu.danhba.fragment.FragmentYeuThich;
import com.example.duytu.danhba.model.Group;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.storeData.ContactManager;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final int PERMISSION_CONTACT = 1;
    TabLayout mTabLayout;
    Toolbar mToolbar;
    public static ViewPager mViewPager;
    public static List<Nguoi> mNguoiList;
    public static List<Group> mExistGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        initToolbar();
        requestPermission();
        initTabLayout();
    }

    private void initTabLayout() {
        mTabLayout.setupWithViewPager(mViewPager);

    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }


    public void setAdapter(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this.getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FragmentDanhBa(), "Danh bạ");
        viewPagerAdapter.addFragment(new FragmentYeuThich(), "Yêu Thích");
        viewPagerAdapter.addFragment(new FragmentNhom(), "Nhóm");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(viewPagerAdapter);
    }

    public void requestPermission() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_CONTACTS)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS}, PERMISSION_CONTACT);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS}, PERMISSION_CONTACT);
            }
        } else {
            ContactManager contactManager = new ContactManager(this);
            mNguoiList = contactManager.getContactData();
            mExistGroup = contactManager.getGroup();
            setAdapter(mViewPager);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CONTACT) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            } else {
                Toast.makeText(this, "Quyền được chấp nhận", Toast.LENGTH_SHORT).show();
                setAdapter(mViewPager);
            }
        }
    }
}

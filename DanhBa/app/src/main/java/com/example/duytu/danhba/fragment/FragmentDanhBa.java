package com.example.duytu.danhba.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.activity.MainActivity;
import com.example.duytu.danhba.adapter.RecyclerViewAdapter;
import com.example.duytu.danhba.interfaces.OnFavouriteClickListener;
import com.example.duytu.danhba.interfaces.OnItemClickListener;
import com.example.duytu.danhba.interfaces.OnItemLongClickListener;


import static com.example.duytu.danhba.activity.MainActivity.mNguoiList;
import static com.example.duytu.danhba.activity.MainActivity.mViewPager;

/**
 * Created by duytu on 5/8/2018.
 */

public class FragmentDanhBa extends Fragment {

    RecyclerView mRvContactList;
    FloatingActionButton mFabAddContact;
    RecyclerViewAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact, container, false);
        mRvContactList = (RecyclerView) view.findViewById(R.id.recyclerView);
        mFabAddContact = (FloatingActionButton) view.findViewById(R.id.fab_add_contact);

        mAdapter = new RecyclerViewAdapter(mNguoiList, getActivity(), R.layout.item_contact);

        setClickListener(mAdapter);

        mRvContactList.setAdapter(mAdapter);

        mRvContactList.setLayoutManager(new
                LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        mRvContactList.setItemAnimator(new DefaultItemAnimator());

        mFabAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, new FragmentThemDanhBa());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public void setClickListener(final RecyclerViewAdapter recyclerViewAdapter) {
        recyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                FragmentChiTiet chiTietFragment = new FragmentChiTiet();
                Bundle bundle = new Bundle();
                bundle.putSerializable("chitiet", mNguoiList.get(position));
                chiTietFragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, chiTietFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        // Xóa thông tin trong CSDL
        recyclerViewAdapter.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public void onLongClick(View view, final int position) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog_function);
                Button btnXoaThongTin = dialog.findViewById(R.id.btn_xoa_thong_tin);
                btnXoaThongTin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mNguoiList.remove(position);
                        // Hàm thay đổi cơ sở dữ liệu
                        /*

                         */

                        ((MainActivity) getActivity()).setAdapter(mViewPager);
                        dialog.dismiss();
                    }
                });

                // Sửa thông tin liên hệ
                Button btnSuaThongTin = dialog.findViewById(R.id.btn_sua_thong_tin);
                btnSuaThongTin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager =
                                getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction =
                                fragmentManager.beginTransaction();
                        FragmentSuaThongTin suaThongTin = new FragmentSuaThongTin();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("sua", mNguoiList.get(position));
                        bundle.putInt("position",position);
                        suaThongTin.setArguments(bundle);
                        fragmentTransaction.replace(R.id.main_layout,suaThongTin);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        dialog.dismiss();
                    }
                });

                // Thêm yêu thích cho liên hệ
                Button btnThemYeuThich = dialog.findViewById(R.id.btn_them_yeu_thich);
                btnThemYeuThich.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mNguoiList.get(position).getFavourite()) {
                            dialog.dismiss();
                        } else {
                            mNguoiList.get(position).setFavourite(true);

                            ((MainActivity) getActivity()).setAdapter(mViewPager);
                            dialog.dismiss();
                        }
                    }
                });

                dialog.show();
            }
        });

        recyclerViewAdapter.setOnFavouriteClickListener(new OnFavouriteClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (mNguoiList.get(position).getFavourite()) {
                    mNguoiList.get(position).setFavourite(false);
                } else mNguoiList.get(position).setFavourite(true);
//                   MainActivity.mViewPager.getAdapter().notifyDataSetChanged();
                ((MainActivity) getActivity()).setAdapter(mViewPager);
            }
        });
    }


}

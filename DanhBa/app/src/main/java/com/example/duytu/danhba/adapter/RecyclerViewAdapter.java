package com.example.duytu.danhba.adapter;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.interfaces.OnFavouriteClickListener;
import com.example.duytu.danhba.interfaces.OnItemClickListener;
import com.example.duytu.danhba.interfaces.OnItemLongClickListener;
import com.example.duytu.danhba.model.Nguoi;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by duytu on 5/8/2018.
 */


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolderItem> {

    Context context;
    Integer layout;
    List<Nguoi> nguoiList;

    OnItemClickListener onItemClickListener;
    OnItemLongClickListener onItemLongClickListener;
    OnFavouriteClickListener onFavouriteClickListener;


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public void setOnFavouriteClickListener(OnFavouriteClickListener onFavouriteClickListener) {
        this.onFavouriteClickListener = onFavouriteClickListener;
    }

    public RecyclerViewAdapter(List<Nguoi> nguoiList, Context context, Integer layout) {
        this.nguoiList = nguoiList;
        this.context = context;
        this.layout = layout;
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {

        ImageView mIvImage;
        TextView mTvName, mTvNumber, mTvEmail;
        ImageView mIvFavourite;

        public ViewHolderItem(View itemView) {
            super(itemView);
            mIvImage = (ImageView) itemView.findViewById(R.id.im_anh);
            mTvName = (TextView) itemView.findViewById(R.id.txt_ten);
            mTvNumber = (TextView) itemView.findViewById(R.id.txt_sdt);
            mTvEmail = (TextView) itemView.findViewById(R.id.txt_email);
            mIvFavourite = (ImageView) itemView.findViewById(R.id.iv_yeuthich);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onClick(v, getLayoutPosition());
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (onItemLongClickListener != null) {
                        onItemLongClickListener.onLongClick(v, getLayoutPosition());
                        return true;
                    }
                    return false;
                }
            });

            mIvFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onFavouriteClickListener != null) {
                        onFavouriteClickListener.onClick(v, getLayoutPosition());
                    }
                }
            });

        }

    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(layout, parent, false);
        return new ViewHolderItem(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolderItem holder, final int position) {

        final Nguoi nguoi = nguoiList.get(position);

        holder.mTvName.setText(nguoi.getName());

        int phoneListSize = nguoi.getPhoneList().size();
        StringBuffer phoneNumber = new StringBuffer();
        for (int i = 0; i < phoneListSize; i++) {
            phoneNumber.append(nguoi.getPhoneList().get(i).getNumber());
            if (i == phoneListSize - 1)
                break;
            phoneNumber.append("\n");
        }
        holder.mTvNumber.setText(phoneNumber.toString());

        if (nguoi.getPhoto() == null) {
            Picasso.with(context).load(R.drawable.user_2).into(holder.mIvImage);
        } else Picasso.with(context).load(nguoi.getPhoto()).into(holder.mIvImage);

        if (nguoi.getEmailList() != null) {
            if (nguoi.getEmailList().size() > 0)
                holder.mTvEmail.setText(nguoi.getEmailList().get(0).getAddress());
            else holder.mTvEmail.setText("");
        } else holder.mTvEmail.setText("");

        if (nguoiList.get(position).getFavourite()) {
            holder.mIvFavourite.setImageResource(R.drawable.star_favourite);
        } else holder.mIvFavourite.setImageResource(R.drawable.star_non_favourite);

    }

    @Override
    public int getItemCount() {
        return nguoiList.size();
    }

}

package com.example.duytu.danhba.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.activity.MainActivity;
import com.example.duytu.danhba.adapter.RecyclerViewAdapter;
import com.example.duytu.danhba.interfaces.OnFavouriteClickListener;
import com.example.duytu.danhba.interfaces.OnItemClickListener;
import com.example.duytu.danhba.interfaces.OnItemLongClickListener;
import com.example.duytu.danhba.model.Nguoi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.example.duytu.danhba.activity.MainActivity.mNguoiList;
import static com.example.duytu.danhba.activity.MainActivity.mViewPager;


/**
 * Created by duytu on 5/8/2018.
 */

public class FragmentYeuThich extends Fragment {

    RecyclerView recyclerView;
    List<Nguoi> nguoiList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favourite, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        if (nguoiList.size() > 0) {
            nguoiList = new ArrayList<>();
        }
        List<Nguoi> ng = MainActivity.mNguoiList;
        for (Nguoi n : ng) {
            if (n.getFavourite()) {
                nguoiList.add(n);
            }
        }
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(nguoiList, getActivity(), R.layout.item_contact);

        setClickListener(adapter);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new
                LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        return view;
    }

    public void setClickListener(final RecyclerViewAdapter recyclerViewAdapter) {
        recyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                FragmentChiTiet chiTietFragment = new FragmentChiTiet();
                Bundle bundle = new Bundle();
                bundle.putSerializable("nguoi", nguoiList.get(position));
                chiTietFragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_layout, chiTietFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        recyclerViewAdapter.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public void onLongClick(View view, final int position) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.dialog_function);

                Button btnXoaThongTin = dialog.findViewById(R.id.btn_xoa_thong_tin);
                btnXoaThongTin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Hàm thay đổi cơ sở dữ liệu
                        /*

                         */

                        Iterator<Nguoi> nguoiIterator = mNguoiList.iterator();
                        while (nguoiIterator.hasNext()) {
                            Nguoi nguoi = nguoiIterator.next();
                            if (nguoi.getName().equals(nguoiList.get(position).getName())) {
                                nguoiIterator.remove();
                            }
                        }
                        nguoiList.remove(position);

                        ((MainActivity) getActivity()).setAdapter(mViewPager);

                        dialog.dismiss();
                    }
                });

                Button btnSuaThongTin = dialog.findViewById(R.id.btn_sua_thong_tin);
                btnSuaThongTin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                Button btnThemYeuThich = dialog.findViewById(R.id.btn_them_yeu_thich);
                btnThemYeuThich.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mNguoiList.get(position).setFavourite(true);

                        /*
                        Chỉnh sửa thông tin trong cơ sở dữ liệu
                         */
                        ((MainActivity) getActivity()).setAdapter(mViewPager);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        recyclerViewAdapter.setOnFavouriteClickListener(new OnFavouriteClickListener() {
            @Override
            public void onClick(View view, int position) {

                nguoiList.remove(position);
                recyclerViewAdapter.notifyDataSetChanged();

                boolean flag = false;
                for (int i = 0; i < mNguoiList.size(); i++) {
                    for (int j = 0; j < nguoiList.size(); j++) {
                        if (mNguoiList.get(i).getName().equals(nguoiList.get(j).getName())) {
                            mNguoiList.get(i).setFavourite(false);
                            flag = true;
                        }
                    }
                    if (flag) break;
                }

                MainActivity.mViewPager.getAdapter().notifyDataSetChanged();


//                if (mNguoiList.get(position).getFavourite()) {
//                    mNguoiList.get(position).setFavourite(false);
//                } else mNguoiList.get(position).setFavourite(true);
//                MainActivity.mViewPager.getAdapter().notifyDataSetChanged();
//                ((MainActivity) getActivity()).setAdapter(mViewPager);
            }
        });


    }
}

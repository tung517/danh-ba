package com.example.duytu.danhba.interfaces;

import android.view.View;

/**
 * Created by duytu on 6/27/2018.
 */

public interface OnItemClickListener {
    void onClick(View view, int position);
}

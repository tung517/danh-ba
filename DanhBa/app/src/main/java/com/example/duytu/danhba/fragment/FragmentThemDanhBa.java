package com.example.duytu.danhba.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.activity.MainActivity;
import com.example.duytu.danhba.model.Address;
import com.example.duytu.danhba.model.Email;
import com.example.duytu.danhba.model.Group;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.model.Phone;
import com.example.duytu.danhba.storeData.InsertData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duytu on 6/8/2018.
 */

public class FragmentThemDanhBa extends Fragment {

    private Button mBtnBack;
    private Button mBtnOk;
    private EditText mEdtName;
    private EditText mEdtPhone;
    private EditText mEdtEmail;
    private EditText mEdtAddress;
    private EditText mEdtOrganization;
    private EditText mEdtBirthDay;
    private Button mBtnAddPhone;
    private Button mBtnAddEmail;
    private Button mBtnAddAddress;
    private Button mBtnAddGroup;
    private Spinner mSpnPhoneType;
    private Spinner mSpnEmailType;
    private Spinner mSpnAddressType;


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.add_contact, container, false);

        mBtnBack = (Button) view.findViewById(R.id.btn_back);
        mBtnOk = (Button) view.findViewById(R.id.btn_ok);
        mEdtName = (EditText) view.findViewById(R.id.edt_name);
        mEdtPhone = (EditText) view.findViewById(R.id.edt_phone_number);
        mEdtEmail = (EditText) view.findViewById(R.id.edt_email);
        mEdtAddress = (EditText) view.findViewById(R.id.edt_address);
        mEdtOrganization = (EditText) view.findViewById(R.id.edt_organization);
        mEdtBirthDay = (EditText) view.findViewById(R.id.edt_birthday);
        mBtnAddPhone = (Button) view.findViewById(R.id.btn_add_phone);
        mBtnAddEmail = (Button) view.findViewById(R.id.btn_add_email);
        mBtnAddAddress = (Button) view.findViewById(R.id.btn_add_address);
        mBtnAddGroup = (Button) view.findViewById(R.id.btn_add_group);
        mSpnPhoneType = (Spinner) view.findViewById(R.id.spn_type_phone);
        mSpnEmailType = (Spinner) view.findViewById(R.id.spn_type_email);
        mSpnAddressType = (Spinner) view.findViewById(R.id.spn_type_address);

        final LayoutInflater vi = (LayoutInflater) getActivity().getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewGroup vgPhone = (ViewGroup) view.findViewById(R.id.ll_add_phone);
        final ViewGroup vgEmail = (ViewGroup) view.findViewById(R.id.ll_add_email);
        final ViewGroup vgAddress = (ViewGroup) view.findViewById(R.id.ll_add_address);
        final ViewGroup vgGroup = (ViewGroup) view.findViewById(R.id.ll_group);

        // List các group hiện tại trong máy
        for (int i = 0; i < MainActivity.mExistGroup.size(); i++) {
            View vGroup = vi.inflate(R.layout.item_group, null);
            CheckBox cbGroup = vGroup.findViewById(R.id.cb_group);
            cbGroup.setText(MainActivity.mExistGroup.get(i).getTitle());
            vgGroup.addView(vGroup, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.FILL_PARENT));
        }

        mBtnAddPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEdtPhone.getText().toString().isEmpty()) {
                    AlertDialog.Builder message = new AlertDialog.Builder(getActivity());
                    message.setTitle("Thông báo");
                    message.setMessage("Bạn chưa điền số điện thoại thứ nhất");
                    message.setPositiveButton("Quay lại", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    message.show();
                } else {
                    final View vw = vi.inflate(R.layout.add_phone, null);
                    EditText edtData = (EditText) vw.findViewById(R.id.edt_data);
                    Spinner spnType = (Spinner) vw.findViewById(R.id.spn_type);
                    ImageView ivRemove = (ImageView) vw.findViewById(R.id.iv_remove);
                    ivRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            vgPhone.removeView(vw);

                        }
                    });
                    vgPhone.addView(vw, new ViewGroup
                            .LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.FILL_PARENT));
                }
            }
        });

        mBtnAddEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEdtEmail.getText().toString().isEmpty()) {
                    AlertDialog.Builder message = new AlertDialog.Builder(getActivity());
                    message.setTitle("Thông báo");
                    message.setMessage("Bạn chưa điền email thứ nhất");
                    message.setPositiveButton("Quay lại", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    message.show();
                } else {
                    final View vw = vi.inflate(R.layout.add_data, null);
                    EditText edtData = (EditText) vw.findViewById(R.id.edt_data);
                    Spinner spnType = (Spinner) vw.findViewById(R.id.spn_type);
                    ImageView ivRemove = (ImageView) vw.findViewById(R.id.iv_remove);
                    edtData.setHint("Email");
                    edtData.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    ivRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            vgEmail.removeView(vw);
                        }
                    });

                    vgEmail.addView(vw, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
                }
            }
        });

        mBtnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEdtAddress.getText().toString().isEmpty()) {
                    AlertDialog.Builder message = new AlertDialog.Builder(getActivity());
                    message.setTitle("Thông báo");
                    message.setMessage("Bạn chưa điền địa chỉ thứ nhất");
                    message.setPositiveButton("Quay lại", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    message.show();
                } else {
                    final View vw = vi.inflate(R.layout.add_data, null);
                    EditText edtData = (EditText) vw.findViewById(R.id.edt_data);
                    Spinner spnType = (Spinner) vw.findViewById(R.id.spn_type);
                    ImageView ivRemove = (ImageView) vw.findViewById(R.id.iv_remove);
                    edtData.setHint("Address");
                    ivRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            vgAddress.removeView(vw);
                        }
                    });
                    vgAddress.addView(vw, new ViewGroup
                            .LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.FILL_PARENT));
                }
            }
        });

        mBtnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog_add_group);
                final EditText nameGroup = dialog.findViewById(R.id.edt_groups_name);
                Button quit = dialog.findViewById(R.id.btn_quit);
                Button ok = dialog.findViewById(R.id.btn_ok);

                quit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (nameGroup.getText().toString().isEmpty()) {
                            AlertDialog.Builder message = new AlertDialog.Builder(getActivity());
                            message.setTitle("Thông báo");
                            message.setMessage("Bạn chưa điền tên nhóm");
                            message.setPositiveButton("Quay lại", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            message.show();
                        } else {
                            InsertData insertData = new InsertData(getActivity());
                            insertData.addNewGroup(nameGroup.getText().toString());
                            dialog.dismiss();
                        }

                    }
                });

                dialog.show();
            }
        });

        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Nguoi nguoi = new Nguoi();

                // Lấy tên người dùng
                if (mEdtName.getText().toString().isEmpty()) {
                    AlertDialog.Builder message = new AlertDialog.Builder(getActivity());
                    message.setTitle("Thông báo");
                    message.setMessage("Bạn chưa điền tên liên lạc");
                    message.setPositiveButton("Quay lại", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    message.show();
                } else {
                    nguoi.setName(mEdtName.getText().toString());

                    // Lấy danh sách điện thoại
                    if (mEdtPhone.getText().toString().isEmpty()) {
                        AlertDialog.Builder message = new AlertDialog.Builder(getActivity());
                        message.setTitle("Thông báo");
                        message.setMessage("Bạn chưa điền số điện thoại");
                        message.setPositiveButton("Quay lại", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        message.show();
                    } else {
                        List<Phone> phoneList = new ArrayList<Phone>();
                        phoneList.add(new Phone(mEdtPhone.getText().toString(),
                                mSpnPhoneType.getSelectedItem().toString()));
                        if (vgPhone.getChildCount() > 0) {
                            int numOfPhone = vgPhone.getChildCount();
                            for (int i = 0; i < numOfPhone; i++) {
                                View newView = vgPhone.getChildAt(i);
                                EditText edtData = (EditText) newView.findViewById(R.id.edt_data);
                                if (edtData.getText().toString().isEmpty())
                                    break;
                                else {
                                    Spinner spnType = (Spinner) newView.findViewById(R.id.spn_type);
                                    phoneList.add(new Phone(edtData.getText().toString(),
                                            spnType.getSelectedItem().toString()));
                                }
                            }
                        }
                        nguoi.setPhoneList(phoneList);


                        // Lấy danh sách Email
                        if (!mEdtEmail.getText().toString().isEmpty()) {
                            List<Email> emailList = new ArrayList<Email>();
                            emailList.add(new Email(mEdtEmail.getText().toString(),
                                    mSpnEmailType.getSelectedItem().toString()));
                            if (vgEmail.getChildCount() > 0) {
                                int count = vgEmail.getChildCount();
                                for (int i = 0; i < count; i++) {
                                    View newView = vgEmail.getChildAt(i);
                                    EditText edtData = (EditText) newView.findViewById(R.id.edt_data);
                                    if (edtData.getText().toString().isEmpty())
                                        break;
                                    else {
                                        Spinner spnType = (Spinner) newView
                                                .findViewById(R.id.spn_type);
                                        emailList.add(new Email(edtData.getText().toString(),
                                                spnType.getSelectedItem().toString()));
                                    }
                                }
                            }
                            nguoi.setEmailList(emailList);
                        }

                        // Lấy danh sách địa chỉ
                        if (!mEdtAddress.getText().toString().isEmpty()) {
                            List<Address> addressList = new ArrayList<>();
                            addressList.add(new Address(mEdtAddress.getText().toString(),
                                    mSpnAddressType.getSelectedItem().toString()));
                            if (vgAddress.getChildCount() > 0) {
                                int count = vgAddress.getChildCount();
                                for (int i = 0; i < count; i++) {
                                    View newView = vgAddress.getChildAt(i);
                                    EditText edtData = (EditText) newView.findViewById(R.id.edt_data);
                                    if (edtData.getText().toString().isEmpty())
                                        break;
                                    else {
                                        Spinner spnType = (Spinner) newView
                                                .findViewById(R.id.spn_type);
                                        addressList.add(new Address(edtData.getText().toString(),
                                                spnType.getSelectedItem().toString()));
                                    }
                                }
                            }
                            nguoi.setAddressList(addressList);
                        }

                        // Lấy tên cơ quan
                        if (!mEdtOrganization.getText().toString().isEmpty()) {
                            nguoi.setOrganization(mEdtOrganization.getText().toString());
                        }

                        // Lấy sinh nhật
                        if (!mEdtBirthDay.getText().toString().isEmpty()) {
                            nguoi.setBirthDay(mEdtBirthDay.getText().toString());
                        }

                        // Lấy nhóm
                        List<Group> groupList = new ArrayList<Group>();
                        for (int i = 0; i < vgGroup.getChildCount(); i++) {
                            View view1 = vgGroup.getChildAt(i);
                            CheckBox cbGroup = view1.findViewById(R.id.cb_group);
                            if (cbGroup.isChecked()) {
                                for (int j = 0; j < MainActivity.mExistGroup.size(); j++) {
                                    if (cbGroup.getText().toString().trim().equals(
                                            MainActivity.mExistGroup.get(j).getTitle().trim())) {
                                        groupList.add(MainActivity.mExistGroup.get(j));
                                    }
                                }
                            }
                        }
                        nguoi.setGroupList(groupList);



                        InsertData insertData = new InsertData(getActivity());
                        Nguoi nguoi1 = insertData.insertNewContact(nguoi);

                        MainActivity.mNguoiList.add(nguoi1);

                        ((MainActivity) getActivity()).setAdapter(MainActivity.mViewPager);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.popBackStack();


                    }
                }
            }
        });

        return view;
    }
}

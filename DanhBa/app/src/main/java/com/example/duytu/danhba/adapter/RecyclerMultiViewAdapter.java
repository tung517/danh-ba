package com.example.duytu.danhba.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duytu.danhba.R;
import com.example.duytu.danhba.interfaces.OnFavouriteClickListener;
import com.example.duytu.danhba.interfaces.OnItemClickListener;
import com.example.duytu.danhba.interfaces.OnItemLongClickListener;
import com.example.duytu.danhba.model.Model;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by duytu on 6/26/2018.
 */

public class RecyclerMultiViewAdapter extends RecyclerView.Adapter {

    Context mContext;
    List<Model> modelList;

    OnItemClickListener onItemClickListener;
    OnItemLongClickListener onItemLongClickListener;
    OnFavouriteClickListener onFavouriteClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public void setOnFavouriteClickListener(OnFavouriteClickListener onFavouriteClickListener) {
        this.onFavouriteClickListener = onFavouriteClickListener;
    }

    public RecyclerMultiViewAdapter(Context mContext, List<Model> modelList) {
        this.mContext = mContext;
        this.modelList = modelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case Model.TYPE_NGUOI:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_contact, parent, false);
                return new NguoiViewHolder(view);
            case Model.TYPE_GROUP:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_other, parent, false);
                return new GroupViewHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Model model = modelList.get(position);
        if (model != null)
            switch (model.getType()) {
                case Model.TYPE_NGUOI:
                    ((NguoiViewHolder) holder).tvName.setText(model.getNguoi().getName());

                    StringBuffer buffer = new StringBuffer();
                    for (int i = 0; i < model.getNguoi().getPhoneList().size(); i++) {
                        buffer.append(model.getNguoi().getPhoneList().get(i).getNumber());
                        if (i == model.getNguoi().getPhoneList().size() - 1)
                            break;
                        buffer.append("\n");
                    }
                    ((NguoiViewHolder) holder).tvPhone.setText(buffer);

                    if (model.getNguoi().getEmailList() != null &&
                            model.getNguoi().getEmailList().size() > 0)
                        ((NguoiViewHolder) holder).tvEmail
                                .setText(model.getNguoi().getEmailList().get(0).getAddress());

                    if (model.getNguoi().getPhoto() == null) {
                        Picasso.with(mContext).load(R.drawable.user_2)
                                .into(((NguoiViewHolder) holder).ivImage);
                    } else {
                        Picasso.with(mContext).load(model.getNguoi().getPhoto())
                                .into(((NguoiViewHolder) holder).ivImage);
                    }
                    break;
                case Model.TYPE_GROUP:
                    ((GroupViewHolder) holder).tvGroup.setText(model.getTitle());
                    break;
            }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class NguoiViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvPhone;
        ImageView ivImage;
        TextView tvEmail;
        ImageView ivFavourite;

        public NguoiViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.txt_ten);
            tvPhone = (TextView) itemView.findViewById(R.id.txt_sdt);
            tvEmail = (TextView) itemView.findViewById(R.id.txt_email);
            ivImage = (ImageView) itemView.findViewById(R.id.im_anh);
            ivFavourite = (ImageView) itemView.findViewById(R.id.iv_yeuthich);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onClick(v, getLayoutPosition());
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (onItemLongClickListener != null) {
                        onItemLongClickListener.onLongClick(v, getLayoutPosition());
                        return true;
                    }
                    return false;
                }
            });

            ivFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onFavouriteClickListener != null) {
                        onFavouriteClickListener.onClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    public class GroupViewHolder extends RecyclerView.ViewHolder {

        TextView tvGroup;

        public GroupViewHolder(View itemView) {
            super(itemView);
            tvGroup = (TextView) itemView.findViewById(R.id.tv_other);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Model model = modelList.get(position);
        switch (model.getType()) {
            case 0:
                return Model.TYPE_NGUOI;
            case 1:
                return Model.TYPE_GROUP;
            default:
                return -1;
        }
    }
}

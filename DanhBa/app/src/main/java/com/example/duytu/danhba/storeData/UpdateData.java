package com.example.duytu.danhba.storeData;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.widget.Toast;

import com.example.duytu.danhba.model.Address;
import com.example.duytu.danhba.model.Email;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.model.Phone;

import java.util.ArrayList;

import static android.R.attr.name;

/**
 * Created by duytu on 6/18/2018.
 */

public class UpdateData {

    public ArrayList<ContentProviderOperation> updateName(
            Nguoi nguoi, String name, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds
                                .StructuredName.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds
                        .StructuredName.DISPLAY_NAME + "= ?", new String[]{nguoi.getName()})
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, "")
                .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, "")
                .withValue(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME, "")
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> updatePhone(
            Nguoi nguoi, int index, Phone phone, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Phone.NUMBER + "= ?",
                        new String[]{nguoi.getPhoneList().get(index).getNumber()})
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone.getNumber())
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, phone.changeType())
                .build());
        return ops;

    }

    public ArrayList<ContentProviderOperation> updateEmail(
            Nguoi nguoi, int index, Email email, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Email.ADDRESS + "= ?",
                        new String[]{nguoi.getEmailList().get(index).getAddress()})
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, email.getAddress())
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE, email.changeType())
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> updateAddress(
            Nguoi nguoi, int index, Address address, ArrayList<ContentProviderOperation> ops) {

        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds
                                .StructuredPostal.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.StructuredPostal.STREET + "= ?",
                        new String[]{nguoi.getAddressList().get(index).getAddress()})
                .withValue(ContactsContract.CommonDataKinds
                        .StructuredPostal.STREET, address.getAddress())
                .withValue(ContactsContract.CommonDataKinds
                        .StructuredPostal.TYPE, address.changeType())
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> updateOrganization(
            Nguoi nguoi, String company, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds
                                .Organization.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Organization.COMPANY + "= ?",
                        new String[]{nguoi.getOrganization()})
                .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, company)
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> insertGroup(
            Nguoi nguoi, String group_row_id, ArrayList<ContentProviderOperation> ops) {

        /*
            Thao tác này là thêm nhóm cho liên lạc chứ không phải đổi tên nhóm,
            các nhóm phải có sẵn để chọn.

            Như vậy là bước tạo nhóm là một bước riêng và tạo trong một hàm riêng.

            Cần có mảng lưu các nhóm có trong máy để lấy id dễ dàng hơn cho việc thêm nhóm.

            Favourite thuộc GroupMembership với GROUP_ROW_ID = 2
         */

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID,
                        Integer.parseInt(nguoi.getRawId()))
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds
                        .GroupMembership.GROUP_ROW_ID, group_row_id)
                .build());
        return ops;
    }

    public ArrayList<ContentProviderOperation> updateBirthday(
            Nguoi nguoi, String birthday, ArrayList<ContentProviderOperation> ops) {
        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "= ?",
                        new String[]{nguoi.getRawId()})
                .withSelection(ContactsContract.Data.MIMETYPE + "= ?",
                        new String[]{ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE})
                .withSelection(ContactsContract.CommonDataKinds.Event.START_DATE + "= ?",
                        new String[]{nguoi.getBirthDay()})
                .withValue(ContactsContract.CommonDataKinds.Event.START_DATE, birthday)
                .build());
        return ops;
    }
}

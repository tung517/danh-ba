package com.example.duytu.danhba.storeData;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.example.duytu.danhba.model.Address;
import com.example.duytu.danhba.model.Email;
import com.example.duytu.danhba.model.Group;
import com.example.duytu.danhba.model.Nguoi;
import com.example.duytu.danhba.model.Phone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duytu on 5/11/2018.
 */

public class ContactManager {

    private Context mContext;

    public ContactManager(Context context) {
        this.mContext = context;
    }

    public List<Nguoi> getContactData() {

        List<Nguoi> nguois = new ArrayList<>();

        ContentResolver contentResolver = mContext.getContentResolver();

        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Cursor cursor = contentResolver.query(uri, null, null, null, DISPLAY_NAME);
        while (cursor.moveToNext()) {
            int has_phone_number = cursor.getInt(cursor.getColumnIndex(HAS_PHONE_NUMBER));
            if (has_phone_number > 0) {
                Nguoi nguoi = new Nguoi();

                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));

                /*

                Name

                 */

                Cursor cursorName = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        ConstantString.NAME_COLUMN,
                        ConstantString.NAME_WHERE + contact_id, null, null);
                while (cursorName.moveToNext()) {

                    String name = cursorName.getString(cursorName.getColumnIndex(
                            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
                    String rawId = cursorName.getString(
                            cursorName.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID));
                    nguoi.setRawId(rawId);
                    nguoi.setName(name);

                }

                /*

                Phone

                 */

                Cursor cursorPhone = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        ConstantString.PHONE_COLUMN,
                        ConstantString.PHONE_WHERE + contact_id, null, null);

                List<Phone> phoneList = new ArrayList<>();

                while (cursorPhone.moveToNext()) {
                    String number = cursorPhone.getString(
                            cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    int phoneType = cursorPhone.getInt(
                            cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    String photo = cursorPhone.getString(
                            cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                    nguoi.setPhoto(photo);

                    if (phoneType == 1) {
                        phoneList.add(new Phone(number, "MOBILE"));
                    } else if (phoneType == 2) {
                        phoneList.add(new Phone(number, "HOME"));
                    } else if (phoneType == 3) {
                        phoneList.add(new Phone(number, "WORK"));
                    } else phoneList.add(new Phone(number, "OTHER"));

                }

                nguoi.setPhoneList(phoneList);

                /*

                Email

                 */

                Cursor cursorEmail = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        ConstantString.EMAIL_COLUMN,
                        ConstantString.EMAIL_WHERE + contact_id, null, null);

                List<Email> emailList = new ArrayList<>();

                while (cursorEmail.moveToNext()) {
                    String email = cursorEmail.getString(
                            cursorEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                    int type = cursorEmail.getInt(
                            cursorEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

                    if (type == 1) {
                        emailList.add(new Email(email, "HOME"));
                    } else if (type == 2) {
                        emailList.add(new Email(email, "WORK"));
                    } else emailList.add(new Email(email, "OTHER"));

                }

                nguoi.setEmailList(emailList);

                /*

                Address

                 */

                Cursor cursorAddress = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        ConstantString.ADDRESS_COLUMN,
                        ConstantString.ADDRESS_WHERE + contact_id, null, null);

                List<Address> addressList = new ArrayList<>();

                while (cursorAddress.moveToNext()) {
                    String address = cursorAddress.getString(
                            cursorAddress.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                    int type = cursorAddress.getInt(
                            cursorAddress.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));

                    if (type == 1) {
                        addressList.add(new Address(address, "HOME"));
                    } else if (type == 2) {
                        addressList.add(new Address(address, "WORK"));
                    } else addressList.add(new Address(address, "OTHER"));

                }

                nguoi.setAddressList(addressList);

                /*

                Organization

                 */

                Cursor cursorOrganization = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        ConstantString.ORGANIZATION_COLUMN,
                        ConstantString.ORGANIZATION_WHERE + contact_id, null, null);

                while (cursorOrganization.moveToNext()) {
                    String organization = cursorOrganization.getString(
                            cursorOrganization.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Organization.COMPANY));
                    nguoi.setOrganization(organization);
                }

                /*

                Group

                 */

                List<Group> groupList = new ArrayList<>();

                Cursor groups = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        new String[]{
                                ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID,
                        },
                        ConstantString.GROUP_WHERE + contact_id,
                        null, null
                );

                while (groups.moveToNext()) {
                    String group_row_id = groups.getString(groups.getColumnIndex(
                            ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID));
                    String wh = ContactsContract.Groups._ID + " = " + group_row_id;

                    Cursor gr = contentResolver.query(
                            ContactsContract.Groups.CONTENT_URI,
                            new String[]{ContactsContract.Groups.TITLE}, wh, null, null);
                    while (gr.moveToNext()) {
                        String title = gr.getString(gr.getColumnIndex(
                                ContactsContract.Groups.TITLE));
                        groupList.add(new Group(title, group_row_id));
                    }
                }

                nguoi.setGroupList(groupList);

                /*

                Favourite

                 */

                Cursor favourites = contentResolver.query(
                        ContactsContract.Contacts.CONTENT_URI,
                        new String[]{
                                ContactsContract.Contacts._ID,
                                ContactsContract.Contacts.STARRED},
                        ConstantString.FAVOURITE_WHERE + contact_id, null, null);
                while (favourites.moveToNext()) {
                    nguoi.setFavourite(true);
                }

                /*

                BirthDay

                 */

                Cursor cursorBirthday = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        ConstantString.BIRTHDAY_COLUMN,
                        ConstantString.BIRTHDAY_WHERE + contact_id, null, null);
                while (cursorBirthday.moveToNext()) {
                    String birthday = cursorBirthday.getString(
                            cursorBirthday.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Event.START_DATE));
                    nguoi.setBirthDay(birthday);
                }

                nguois.add(nguoi);
            }
        }
        return nguois;
    }

    public List<Group> getGroup() {

        List<Group> groupList = new ArrayList<>();

        String where = ContactsContract.Groups._ID + "!=" + "'2'";
        String[] column = {
                ContactsContract.Groups._ID,
                ContactsContract.Groups.TITLE
        };
        Cursor cursor = mContext.getContentResolver().query(
                ContactsContract.Groups.CONTENT_URI, null, where, null, null);
        while (cursor.moveToNext()) {
            String group_row_id = cursor.getString(cursor.getColumnIndex(ContactsContract.Groups._ID));
            String tittle = cursor.getString(cursor.getColumnIndex(ContactsContract.Groups.TITLE));
            groupList.add(new Group(tittle, group_row_id));
        }

        return groupList;
    }


}
